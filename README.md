# Cloud Deploy

Docker images for deployments on major cloud platforms (GDC, Azure, AWS)

---

| Service  | Implemented  |  Details |
|-------|-----|---|
| AWS   | :heavy_check_mark:  | AWS CLI (version 2.x)  |
| Azure | :x:  |   |
| GDC   | :x:  |   |

The following projects depend on `cloud-deploy`:
- [Status Page](https://gitlab.com/gitlab-org/status-page)
- [GitLab](htps://gitlab.com/gitlab-org/gitlab) (as of [GitLab 12.9](https://gitlab.com/gitlab-org/gitlab/issues/2079620) )
